import React, { Component } from "react";

import { Platform, StyleSheet, Text, View } from "react-native";
export default class Campo extends Component<Props> {
	render() {
		if (this.props.showCase) {
			return <Text style={styles.welcome}>Maracatu Atomico !</Text>;
		} else {
			return null;
		}
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#F5FCFF"
	},
	welcome: {
		fontSize: 20,
		textAlign: "center",
		margin: 10
	},
	instructions: {
		textAlign: "center",
		color: "#333333",
		marginBottom: 5
	}
});
